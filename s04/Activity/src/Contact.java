import java.util.ArrayList;

public class Contact {
    private String name;
    private ArrayList<String> contactNumbers;
    private String address;

    public Contact() {
        contactNumbers = new ArrayList<>();
    }

    public Contact(String name, String contactNumber, String address) {
        this.name = name;
        this.contactNumbers = new ArrayList<>();
        this.contactNumbers.add(contactNumber);
        this.address = address;
    }

    // Setters and getters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getContactNumbers() {
        return contactNumbers;
    }

    public void setContactNumbers(ArrayList<String> contactNumbers) {
        this.contactNumbers = contactNumbers;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
