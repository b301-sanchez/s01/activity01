package com.zuitt.example;

public class Car {

    //[] Composition
    // make Driver a component of a Car / one of properties of Car
    private Driver d;

    //[]Encapsulation
    // encapsulation is a technique that involves hiding data by making variables private and providing public methods to access and modify them. It ensures data security and allows for controlled interaction with objects, promoting code organization and flexibility.
    private String name;
    private String brand;
    private int yearOfMake;

    // constructors
    public Car(){
        this.d = new Driver("Alejandro");
        // Whenever we create a new car, it will have a driver named Alejandro.
    }

    public Car(String name, String brand, int yearOfMake){
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
        this.d = new Driver("Alejandro");
    }

    // setters
    public void setCarName(String name){
        this.name = name;
    }

    public void setBrand(String brand){
        this.brand = brand;
    }

    public void setYearOfMake(int yearOfMake){
        this.yearOfMake = yearOfMake;
    }


    // getters
    public String getCarName(){
        return name;
    }

    public String getBrand(){
        return brand;
    }

    public int getYearOfMake(){
        return yearOfMake;
    }

    public String getDriver(){
        return this.d.getName();
        // Car.Driver.getName()
    }



}
