package com.zuitt.example;

//"implements" keyword is used to establish between class and an interface
public class Person implements Actions{
    public void sleep(){
        System.out.println("Zzzzzzzz");
    }
    public void run(){
        System.out.println("running");
    }
}
