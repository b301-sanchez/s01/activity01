package com.zuitt.example;
//[] Abstraction
//Declares method without providing implementation. It serves as blueprint or contract that other classes can follow
//Usng the interface, we could define method signatures (blueprints) that must be implemented by classes
public interface Actions {
    public void sleep();
    public void run();
}