package com.zuitt.example;

//[] Inheritance
//"extend" keyword in Java is to establish inheritance between classes
public class Dog extends Animal{

    private String breed;
    //inheritance is a concept in programming where a class can inherit the properties and behaviours and methods of another class.
    public Dog(){
        super(); //Animal() constructor
        this.breed = "Chihuahua";

    }

    public Dog(String name, String color, String breed){
        super(name, color);// Animal(String name, String color) constructor
        this.breed = breed;

    }
    //getter
    public String getBreed(){
        return this.breed;
    }

    public void setBreed(String breed){
        this.breed = breed;
    }
    public void printDogInfo(){
        System.out.println("Name: " +getName());
        System.out.println("Color: " + getColor());
        System.out.println("Breed: " + breed);
    }

}
