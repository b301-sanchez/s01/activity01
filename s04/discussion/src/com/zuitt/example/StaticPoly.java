package com.zuitt.example;
// Polymorphism is the ability to exhibit different behaviours.
// Static Polymorphism (method overloading) allows different behaviors with the same name, different parameters and compile time;

// Static Polymorphism is achieved through method overloading, where multiple methods with same name but different parameters existing in the same class.
public class StaticPoly {

    public int addition(int a, int b){
        return a + b;
    }
    public int addition(int a, int b, int c){
        return a + b + c;
    }
    public double addition(double a, double b){
        return a + b;
    }
}
