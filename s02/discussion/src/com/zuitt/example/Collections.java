package com.zuitt.example;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Collections {
    public static void main (String[] args){
        // Arrays are fixed size data structures that store a collection of elements of the same data type

        int [] intArray = new int[5];
        System.out.println("Initial state of intArray: ");
        System.out.println(Arrays.toString(intArray));
        //  1  2   3  4  5
        // [0, 0 , 0, 0, 0]
        intArray[0] = 1;
        intArray[intArray.length-1] = 5;
        System.out.println("Undated Array: ");
        System.out.println(Arrays.toString(intArray));
        // Could not add elements because the length is already fixed.
        // intArray[intArray.length] = 6;
        // System.out.println(Arrays.toString(intArray));

        // Multidimensional Arrays
        // 3 rows, 3 columns
        String[][] classroom = new String [3][3];

        //first row
        classroom[0][0]= "Athos";
        classroom[0][1]= "Porthos";
        classroom[0][2]= "Aramis";

        //second row
        classroom[1][0]= "Brandon";
        classroom[1][1]= "Junjun";
        classroom[1][2]= "Jobert";

        //Third row
        classroom[2][0]= "Mickey";
        classroom[2][1]= "Donald";
        classroom[2][2]= "Goofy";

        System.out.println(Arrays.deepToString(classroom));

        // ArrayLists are dynamic data structures that can grow or shrink in size as needed.
        System.out.println("--------------------");
        ArrayList<String> students = new ArrayList<String>();
        System.out.println(students);

        students.add("John");
        students.add("Paul");

        // size() is to retrieve the size of the array list.
        System.out.println(students.size()); //2

        // get () is to retrieve an item using an index
        System.out.println(students.get(0)); //John
        System.out.println(students.get(1));
        System.out.println(students);

        // set() updating element/item
        students.set(1, "George");
        System.out.println(students);

        // remove() is to delete/remove an element using specific index
        students.remove(1);
        System.out.println(students);

        // method to empty the ArrayList
        students.clear();
        System.out.println(students);


        System.out.println("--------------------");
        // HashMaps are key-value pair data structures that provide a way to store and retrieve elements based on unique keys.

        HashMap<String, String> job_position = new HashMap<String, String>();

                        //key       //value
        job_position.put("Brandon", "Student");
        job_position.put("Alice", "Dreamer");
        System.out.println(job_position);


        // Retrieving value using key
        System.out.println(job_position.get("Alice"));

        job_position.put("Brandon", "Driver");
        System.out.println(job_position);

        // Deleting/removing a value using key
        // job_position.remove("Brandon");
        //System.out.println(job_position);

        System.out.println(job_position.keySet());
        System.out.println(job_position.values());

    }
}
