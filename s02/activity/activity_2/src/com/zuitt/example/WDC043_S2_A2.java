package com.zuitt.example;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class WDC043_S2_A2 {
    public static void main (String[] args){

        int [] intArray = new int[5];
        System.out.println(Arrays.toString(intArray));
        intArray[0] = 2;
        intArray[1] = 3;
        intArray[2] = 5;
        intArray[3] = 7;
        intArray[4] = 11;
        System.out.println(Arrays.toString(intArray));

        System.out.println("The first prime number is "+ intArray[0]);
        System.out.println("The third prime number is "+ intArray[2]);
        System.out.println("The last prime number is "+ intArray[4]);

        System.out.println("--------------------");
        ArrayList<String> friends = new ArrayList<String>();


        friends.add("Jon");
        friends.add("Arya");
        friends.add("Robb");
        friends.add("Sansa");

        System.out.println("My friends are " + friends);

        System.out.println("--------------------");
        HashMap<String, String> inventory = new HashMap<String, String>();

        inventory.put("toothpaste", "15");
        inventory.put("toothbrush", "20");
        inventory.put("soap", "12");

        System.out.println("Our current inventory consists of : " + inventory);

    }
}
