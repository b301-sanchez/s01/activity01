package com.zuitt.example;

import java.util.Scanner;

public class GradeAverage {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.println("How old are you? ");

        // if we need to convert it to be a numerical data type such as double, we can pass the Scanner input as an argument to the Double class Constructor
        double age = new Double(userInput.nextLine());
        System.out.println("This is a confirmation that you are " + age + " years old");
    }
}
