package com.zuitt.example;

public class HelloWorld {
    // In Java every standalone program must have a 'main method' as the starting point for the execution of the program.

    /*
      * - public: It is an access specifier that indicates the method is accessible from anywhere in the program
      * - public: It means that the method belongs to the class itself rather tan instance oft the class. The method needs to be static because it is called by a Java run time system before any objects are created
      * - void: it is the return type for the method indication that the main method does  not return anything
      * - main: It is the name of the method. Tha java runtime system looks for this exact method to start the program
      * - (String[] args): a parameter severs as a way for java program to receive information or instruction from the outside when it starts running
     */
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }

}
