package com.zuitt.activity;
import java.util.Scanner;


public class AverageGrade {
    public static void main(String[] args) {

        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;
        double average;


        Scanner myObj = new Scanner(System.in);
        System.out.println("First name:");
        firstName = myObj.nextLine();

        System.out.println("Last name:");
        lastName = myObj.nextLine();

        System.out.println("First Subject Grade:");
        firstSubject = myObj.nextDouble();

        System.out.println("Second Subject Grade:");
        secondSubject = myObj.nextDouble();

        System.out.println("Third Subject Grade:");
        thirdSubject = myObj.nextDouble();

        average = (firstSubject + secondSubject + thirdSubject) / 3;

        System.out.println("Good day, " + firstName + " " + lastName );
        System.out.println("Average Grade: " + average);


    }
}