public class Main {
    public static void main(String []args){

        //For loops
            // i=0 intial/starting value
            //i<10 - condition to be satisfied
            //i ++ - change of value
        for(int i=0; i<10; i++){
            System.out.println("Cureent count: " + i);

        }

        int [] hundreds = {100, 200, 300, 400, 500};
        for(int i=0; i < hundreds.length; i++){
            // 0 true, hundreds[0] = 100, i++ >> 1
            // 1 true, hundreds[1] = 200, i++ >> 2
            // 2 true, hundreds[2] = 300, i++ >> 3
            // 3 true, hundreds[3] = 400, i++ >> 4
            // 4 true, hundreds[4] = 500, i++ >> 5
            // 5 false end
            System.out.println(hundreds[i]);
        }

        System.out.println("Other approach---");
        for(int hundred: hundreds){
            System.out.println(hundred);


        }
        //While
        int i = 0;

        while (i <10){
            System.out.println("Current count: "+ i);
            i++;
        }

        // do while - runs atleast once
        int y =0;
        do{
            System.out.println("Y value " + y);
        }
        while (y !=0);
    }
}