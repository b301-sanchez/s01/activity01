import java.util.Scanner;

public class ExeceptionHandling {
    public static void main (String[] args){

        Scanner input = new Scanner(System.in);
        int num = 0;

        System.out.println("Input a number");
        try {
            num = input.nextInt();
        } catch (Exception e){ // is a variable representing an exception object
            System.out.println("Invalid input, this program only accepts interger inputs. ");
            e.printStackTrace(); // a way to show information about an error occured in Java
        }
        System.out.println("The number you entered is: "+ num);
        System.out.println("Next line of codes");

    }
}
