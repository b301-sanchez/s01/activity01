import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main (String[] args){

        Scanner input = new Scanner(System.in);


        System.out.println("Input an integer whose factorial will be computed");
        try {
            int num = input.nextInt();
            int factorial = 1;
            int x =1;

            while (x <= num) {
                factorial *= x;
                x++;
            }
            if (num == 0) {
                factorial = 1;
            } else if (num < 0) {
                System.out.println("Cannot compute for factorials of negative numbers.");
                return;
            }



            //for (int i = 1; i <= num; i++) {
              //  factorial *= i;
            //}
            System.out.println("The factorial of "+ num + " is " + factorial);

        } catch (Exception e){
            e.printStackTrace();
        }

    }
}

